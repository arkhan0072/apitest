<?php

use App\Http\Controllers\OrderController;
use App\Http\Resources\BannerResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\DistrictResource;
use App\Http\Resources\ProductResource;
use App\Models\Banner;
use App\Models\Category;
use App\Models\City;
use App\Models\District;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([
    'middleware' => 'api',
    'prefix' => 'v1'
], function ($router) {
    //products api
    Route::get('/product/{id}', function ($id) {
        return new ProductResource(Product::WithAll()->findOrFail($id));
    });
    Route::get('/products', function () {
        return ProductResource::collection(Product::where('is_featured',1)->get());
    });
    //categories
    Route::get('/categories', function () {
        return CategoryResource::collection(Category::all());
    });
    //category/products
    Route::get('/category/{id}/products', function ($id) {
        return ProductResource::collection(Product::where('category_id',$id)->get());
    });
    //order process
    Route::post('order/placed',[OrderController::class,'store'])->name('orderStore');
});
