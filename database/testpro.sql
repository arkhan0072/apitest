-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2021 at 05:35 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testpro`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bed Room', '2021-03-30 00:20:27', '2021-03-30 00:20:27'),
(2, 'Living Room', '2021-03-30 00:20:27', '2021-03-30 00:20:27'),
(3, 'DSLR Camera', '2021-03-30 00:20:27', '2021-03-30 00:20:27'),
(4, 'Appliances', '2021-03-30 00:20:27', '2021-03-30 00:20:27'),
(5, 'Storage', '2021-03-30 00:20:27', '2021-03-30 00:20:27'),
(6, 'Packages', '2021-03-30 00:20:27', '2021-03-30 00:20:27');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_26_163344_create_products_table', 1),
(5, '2021_03_26_163354_create_orders_table', 1),
(6, '2021_03_26_163404_create_banners_table', 1),
(7, '2021_03_26_163421_create_orderitems_table', 1),
(8, '2021_03_26_192657_create_districts_table', 2),
(9, '2021_03_26_192740_create_cities_table', 2),
(10, '2021_03_31_142700_create_product_details_table', 3),
(11, '2021_03_31_143106_create_product_sizes_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `order_id`, `product_id`, `price`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 9, 1, '100.00', 5, '2021-03-26 15:42:53', '2021-03-26 15:42:53'),
(2, 9, 2, '100.00', 5, '2021-03-26 15:42:53', '2021-03-26 15:42:53'),
(3, 10, 3, '100.00', 5, '2021-03-30 10:05:39', '2021-03-30 10:05:39'),
(4, 11, 4, '100.00', 5, '2021-03-30 10:06:05', '2021-03-30 10:06:05'),
(5, 12, 1, '100.00', 5, '2021-03-30 10:06:21', '2021-03-30 10:06:21'),
(6, 12, 1, '100.00', 5, '2021-03-30 10:06:21', '2021-03-30 10:06:21'),
(7, 13, 1, '100.00', 5, '2021-03-30 10:07:20', '2021-03-30 10:07:20'),
(8, 13, 1, '100.00', 5, '2021-03-30 10:07:20', '2021-03-30 10:07:20'),
(9, 14, 1, '100.00', 5, '2021-03-30 10:15:15', '2021-03-30 10:15:15'),
(10, 14, 1, '100.00', 5, '2021-03-30 10:15:15', '2021-03-30 10:15:15'),
(11, 15, 1, '100.00', 5, '2021-03-30 10:16:06', '2021-03-30 10:16:06'),
(12, 15, 1, '100.00', 5, '2021-03-30 10:16:06', '2021-03-30 10:16:06'),
(13, 16, 1, '100.00', 5, '2021-03-30 10:17:51', '2021-03-30 10:17:51'),
(14, 16, 1, '100.00', 5, '2021-03-30 10:17:52', '2021-03-30 10:17:52'),
(15, 17, 1, '100.00', 5, '2021-03-30 10:18:17', '2021-03-30 10:18:17'),
(16, 17, 1, '100.00', 5, '2021-03-30 10:18:17', '2021-03-30 10:18:17'),
(17, 18, 1, '100.00', 5, '2021-03-30 10:18:34', '2021-03-30 10:18:34'),
(18, 18, 1, '100.00', 5, '2021-03-30 10:18:34', '2021-03-30 10:18:34'),
(19, 19, 1, '100.00', 5, '2021-03-30 10:33:37', '2021-03-30 10:33:37'),
(20, 19, 1, '100.00', 5, '2021-03-30 10:33:37', '2021-03-30 10:33:37'),
(21, 20, 1, '100.00', 5, '2021-03-30 10:36:26', '2021-03-30 10:36:26'),
(22, 20, 1, '100.00', 5, '2021-03-30 10:36:26', '2021-03-30 10:36:26'),
(23, 21, 1, '100.00', 5, '2021-03-30 10:36:51', '2021-03-30 10:36:51'),
(24, 21, 1, '100.00', 5, '2021-03-30 10:36:52', '2021-03-30 10:36:52'),
(25, 22, 1, '100.00', 5, '2021-03-30 10:46:13', '2021-03-30 10:46:13'),
(26, 22, 1, '100.00', 5, '2021-03-30 10:46:13', '2021-03-30 10:46:13'),
(27, 23, 1, '100.00', 5, '2021-03-30 10:53:23', '2021-03-30 10:53:23'),
(28, 23, 1, '100.00', 5, '2021-03-30 10:53:23', '2021-03-30 10:53:23'),
(29, 24, 1, '100.00', 5, '2021-03-30 10:56:07', '2021-03-30 10:56:07'),
(30, 24, 1, '100.00', 5, '2021-03-30 10:56:08', '2021-03-30 10:56:08'),
(31, 25, 1, '100.00', 5, '2021-03-30 10:57:36', '2021-03-30 10:57:36'),
(32, 25, 1, '100.00', 5, '2021-03-30 10:57:36', '2021-03-30 10:57:36'),
(33, 26, 1, '100.00', 5, '2021-03-30 10:58:57', '2021-03-30 10:58:57'),
(34, 26, 1, '100.00', 5, '2021-03-30 10:58:57', '2021-03-30 10:58:57'),
(35, 27, 1, '100.00', 5, '2021-03-30 11:02:28', '2021-03-30 11:02:28'),
(36, 27, 1, '100.00', 5, '2021-03-30 11:02:28', '2021-03-30 11:02:28'),
(37, 28, 1, '100.00', 5, '2021-03-31 10:25:07', '2021-03-31 10:25:07'),
(38, 28, 1, '100.00', 5, '2021-03-31 10:25:07', '2021-03-31 10:25:07'),
(39, 29, 1, '100.00', 5, '2021-03-31 10:26:00', '2021-03-31 10:26:00'),
(40, 29, 1, '100.00', 5, '2021-03-31 10:26:00', '2021-03-31 10:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `delivery_date` date NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_no`, `total_price`, `delivery_date`, `type`, `name`, `email`, `phone`, `address`, `district_id`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'JKD-2021-03-26 08:36:56-n', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:36:56', '2021-03-26 15:36:56'),
(2, 'JKD-2021-03-26 08:37:42-N', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:37:42', '2021-03-26 15:37:42'),
(3, 'JKD-2021-03-26 08:39:36-I', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:39:36', '2021-03-26 15:39:36'),
(4, 'JKD-2021-03-26 08:40:00-B', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:40:00', '2021-03-26 15:40:00'),
(5, 'JKD-2021-03-26 08:40:13-r', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:40:14', '2021-03-26 15:40:14'),
(6, 'JKD-2021-03-26 08:41:17-i', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:41:17', '2021-03-26 15:41:17'),
(7, 'JKD-2021-03-26 08:41:51-3', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:41:51', '2021-03-26 15:41:51'),
(8, 'JKD-2021-03-26 08:42:17-T', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:42:17', '2021-03-26 15:42:17'),
(9, 'JKD-2021-03-26 08:42:53-H', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-26 15:42:53', '2021-03-26 15:42:53'),
(10, 'JKD-2021-03-30 03:05:38-P', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:05:38', '2021-03-30 10:05:38'),
(11, 'JKD-2021-03-30 03:06:05-G', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:06:05', '2021-03-30 10:06:05'),
(12, 'JKD-2021-03-30 03:06:21-a', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:06:21', '2021-03-30 10:06:21'),
(13, 'JKD-2021-03-30 03:07:20-n', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:07:20', '2021-03-30 10:07:20'),
(14, 'JKD-2021-03-30 03:15:15-4', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:15:15', '2021-03-30 10:15:15'),
(15, 'JKD-2021-03-30 03:16:06-7', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:16:06', '2021-03-30 10:16:06'),
(16, 'JKD-2021-03-30 03:17:51-r', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:17:51', '2021-03-30 10:17:51'),
(17, 'JKD-2021-03-30 03:18:17-B', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:18:17', '2021-03-30 10:18:17'),
(18, 'JKD-2021-03-30 03:18:33-s', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:18:33', '2021-03-30 10:18:33'),
(19, 'JKD-2021-03-30 03:33:37-k', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:33:37', '2021-03-30 10:33:37'),
(20, 'JKD-2021-03-30 03:36:26-R', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:36:26', '2021-03-30 10:36:26'),
(21, 'JKD-2021-03-30 03:36:51-y', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:36:51', '2021-03-30 10:36:51'),
(22, 'JKD-2021-03-30 03:46:13-T', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:46:13', '2021-03-30 10:46:13'),
(23, 'JKD-2021-03-30 03:53:23-p', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:53:23', '2021-03-30 10:53:23'),
(24, 'JKD-2021-03-30 03:56:07-N', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:56:07', '2021-03-30 10:56:07'),
(25, 'JKD-2021-03-30 03:57:36-6', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:57:36', '2021-03-30 10:57:36'),
(26, 'JKD-2021-03-30 03:58:57-v', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 10:58:57', '2021-03-30 10:58:57'),
(27, 'JKD-2021-03-30 04:02:28-0', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-30 11:02:28', '2021-03-30 11:02:28'),
(28, 'JKD-2021-03-31 03:25:07-0', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-31 10:25:07', '2021-03-31 10:25:07'),
(29, 'JKD-2021-03-31 03:26:00-E', '1000.00', '2021-02-02', 'one time', 'Abdulrehman Khan', NULL, '03478035100', 'test test test', 1, 1, '2021-03-31 10:26:00', '2021-03-31 10:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `price`, `picture`, `is_featured`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Product 1', 'product-1', '100.00', 'http://127.0.0.1:8000/product/product1.png', 1, 1, '2021-03-26 19:41:07', '2021-03-26 19:41:07'),
(2, 'Product 2', 'product-2', '150.00', 'http://127.0.0.1:8000/product/product2.png', 1, 2, '2021-03-26 19:41:07', '2021-03-26 19:41:07'),
(3, 'Product 3', 'product-3', '100.00', 'http://127.0.0.1:8000/product/product3.png', 1, 1, '2021-03-26 19:41:07', '2021-03-26 19:41:07'),
(4, 'Product 4', 'product-4', '100.00', 'http://127.0.0.1:8000/product/product4.png', 1, 1, '2021-03-26 19:41:07', '2021-03-26 19:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `refundable_amount` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`id`, `product_id`, `type`, `basis`, `refundable_amount`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rent', 'Monthly', '1800.00', '2021-03-31 14:35:14', '2021-03-31 14:35:14'),
(2, 2, 'Buy', 'One Time', '0.00', '2021-03-31 14:35:14', '2021-03-31 14:35:14'),
(3, 3, 'Rent', 'Weekly', '1200.00', '2021-03-31 14:35:14', '2021-03-31 14:35:14'),
(4, 4, 'Instalment', 'Monthly', '100.00', '2021-03-31 14:35:14', '2021-03-31 14:35:14');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `product_id`, `size`, `created_at`, `updated_at`) VALUES
(1, 1, '6x3', '2021-03-31 14:36:58', '2021-03-31 14:36:58'),
(2, 1, '6x4', '2021-03-31 14:36:58', '2021-03-31 14:36:58'),
(3, 1, '6x5', '2021-03-31 14:36:58', '2021-03-31 14:36:58'),
(4, 1, '6x6', '2021-03-31 14:36:58', '2021-03-31 14:36:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
