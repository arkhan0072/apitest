<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_no');
            $table->decimal('total_price',10,2);
            $table->date('delivery_date');
            $table->string('type');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone');
            $table->string('address',500);
            $table->integer('district_id')->index('district_id');
            $table->integer('city_id')->index('city_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
