<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    public function orderitems(){
        return $this->hasMany(Orderitem::class,'order_id');
    }

    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }
    public function scopeWithAll($query)
    {
        return $query->with('orderitems','city');
    }
}
