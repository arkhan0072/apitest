<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderitem extends Model
{
    use HasFactory;
    public function order(){
       return $this->belongsTo(Order::Class);
    }
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
    public function scopeWithAll($query)
    {
        return $query->with('product');
    }

}
