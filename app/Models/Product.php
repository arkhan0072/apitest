<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function detail(){
        return $this->hasOne(ProductDetail::class,'product_id');
    }
    public function sizes(){
        return $this->hasMany(ProductSize::class,'product_id');
    }
    public function orderitem(){
        return $this->belongsTo(Orderitem::class);
    }
    public function scopeWithAll($query)
    {
        return $query->with('detail','sizes');
    }
}
