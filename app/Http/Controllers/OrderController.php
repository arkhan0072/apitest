<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Orderitem;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $order = new Order();
            $order->order_no = "JKD-" . Carbon::now('Y') . "-" . Str::random(1, 200);
            $order->total_price = $request->total_price;
            $order->delivery_date = Carbon::parse($request->delivery_date)->toDateString();
            $order->type = $request->type;
            $order->name = $request->name;
            if ($request->email) {
                $order->email = $request->email;
            }
            $order->phone = $request->phone;
            $order->address = $request->address;
            $order->district_id = $request->district_id;
            $order->city_id = $request->city_id;
            $order->save();
            $orderitems = json_decode($request->orderitems);
//            return response()->json($orderitems);
            $show=[];
            foreach ($orderitems as $orderitem) {
                $orderitemz = new Orderitem();
                $orderitemz->order_id = $order->id;
                $orderitemz->product_id = $orderitem->product_id;
                $orderitemz->price = $orderitem->price;
                $orderitemz->quantity = $orderitem->quantity;
                $orderitemz->save();
                array_push($show,$orderitemz->load('product'));
            }
//            $api_key = "923005770100-358b7e1d-ce5f-4e52-861b-cdd8a04e1019";///YOUR API KEY
//            $mobile = "923005770100";///Recipient Mobile Number
//            $sender = "JK Dairies";
//            $message = "Your order has been placed on the JK Dairies your order number: ".$order->order_no." and you order :";
//            $message.="<br>";
//                foreach ($show as $s) {
//                    $message .= "Product Name :" . $s->name . "<br>";
//                }
//            $message.="Total Price of your order ".$order->total;
//            $url = "https://sendpk.com/api/sms.php?username=923005770100&password=P@kistan7878";
//            $post = "sender=".urlencode($sender)."&mobile=".urlencode($mobile)."&message=".urlencode($message)."";
//            $ch = curl_init();
//            $timeout = 10; // set to zero for no timeout
//            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
//            curl_setopt($ch, CURLOPT_URL,$url);
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
//            $result = curl_exec($ch);
            return response(['data' => $order->load('orderitems')]);
        } catch (Exception $e) {
            return response(['data' => $e->getMessage()]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @return Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @return Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Order $order
     * @return Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
