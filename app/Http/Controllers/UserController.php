<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function dashboard(){
        $orders=Order::whereHas('orderitems')->WithAll()->get();
        $totalCount=Order::count();
        $total=Order::where('created_at',Carbon::now()->toDateString())->sum('total_price');
        $categories=Category::all();
        $products=Product::all();
        $average=($total/$orders->count())/100;
        return view('dashboard',compact('orders','average','total','totalCount','categories','products'));
    }
}
